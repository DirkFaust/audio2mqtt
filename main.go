package main

import (
	"fmt"
	"math"
	"os"
	"os/signal"
	"sync"
	"time"

	"github.com/gordonklaus/portaudio"
	"github.com/joho/godotenv"
	log "github.com/sirupsen/logrus"
	"gitlab.com/DirkFaust/faustility/pkg/audiocrypt"
	"gitlab.com/DirkFaust/faustility/pkg/conversion"
	"gitlab.com/DirkFaust/faustility/pkg/env"
	"gitlab.com/DirkFaust/faustility/pkg/mqtt"
)

const (
	envLogLevel         = "LOG_LEVEL"
	envMqttHost         = "MQTT_HOST"
	envMqttPort         = "MQTT_PORT"
	envAudioDeviceIndex = "AUDIO_DEVICE_INDEX"
	envPassword         = "ENCRYPTION_PASSWORD"
)

type audioPacket struct {
	data []byte
}

func (a audioPacket) Serialize() (*[]byte, error) {
	return &a.data, nil
}

func getAudioDevice(index int) (*portaudio.DeviceInfo, error) {
	devs, err := portaudio.Devices()
	if err != nil {
		return nil, err
	}

	if index > len(devs) {
		return nil, fmt.Errorf("audio device index out of range. Devices available: %v", devs)
	}

	return devs[index], nil

}

func OpenAudioInputStream(inDev *portaudio.DeviceInfo, numInputChannels int, sampleRate float64, framesPerBuffer int, args ...interface{}) (*portaudio.Stream, error) {
	p := portaudio.HighLatencyParameters(inDev, nil)
	p.Input.Channels = numInputChannels
	p.Output.Channels = 0
	p.SampleRate = sampleRate
	p.FramesPerBuffer = framesPerBuffer
	return portaudio.OpenStream(p, args...)
}

func audioLevel(pcm []int16) int {
	const max int16 = 32767

	var lastMax int16 = 0
	for _, i := range pcm {
		if i > lastMax {
			lastMax = i
		}
	}

	return int(math.Round(100.0 / float64(max) * float64(lastMax)))
}

func main() {
	_ = godotenv.Load()

	log.SetFormatter(&log.TextFormatter{FullTimestamp: true})
	log.SetOutput(os.Stdout)
	level, err := log.ParseLevel(env.GetOrDefault(envLogLevel, "info"))
	if err != nil {
		level = log.InfoLevel
	}
	log.SetLevel(level)

	var audioDeviceIndex int
	var mqttPort uint16
	conversion.FromStringOrDie(env.GetOrDie(envAudioDeviceIndex), &audioDeviceIndex)
	conversion.FromStringOrDie(env.GetOrDefault(envMqttPort, "1883"), &mqttPort)
	mqttHost := env.GetOrDie(envMqttHost)

	audioData := make(chan audioPacket)
	defer close(audioData)
	kill := make(chan bool)
	defer close(kill)

	forwarder, err := mqtt.NewForwarderBuilder[audioPacket]().
		Channels().Data(audioData).KillWitch(kill).
		Topics().Input("audio2mqtt/command").Output(fmt.Sprintf("audio2mqtt/pcm/%d", audioDeviceIndex)).
		Server().Host(mqttHost).Port(uint16(mqttPort)).
		Build()

	if err != nil {
		log.Errorf("Unable to build MQTT forwarder: %v", err)
		os.Exit(1)
	}

	password := os.Getenv(envPassword)
	doEncrypt := (len(password) > 0)
	var encryptor audiocrypt.AudioEncryptor

	if !doEncrypt {
		log.Warn("NOT using encryption - its highly unadviced to send your permanent(!) recorded(!) audio unencrypted. Even if the MQTT is secured via TLS, EVERY client on that server can receive AND HEAR the audio.")
	} else {
		encryptor = audiocrypt.NewAudioEncryptor(password, audiocrypt.RandomSalt())
	}

	all := 0
	ticker := time.NewTicker(time.Second * 1)

	wg := sync.WaitGroup{}
	// We just wait for one as one exit should end the whole process.
	wg.Add(1)

	go func() {
		signalCh := make(chan os.Signal, 1)
		signal.Notify(signalCh, os.Interrupt)
		defer close(signalCh)

		portaudio.Initialize()
		defer portaudio.Terminate()

		audioDevice, err := getAudioDevice(audioDeviceIndex)
		if err != nil {
			panic(err)
		}

		log.Infof("Using audio device: %s at index %d", audioDevice.Name, audioDeviceIndex)

		pcm := make([]int16, 512)
		stream, err := OpenAudioInputStream(audioDevice, 1, 16000, len(pcm), pcm)

		if err != nil {
			log.Fatalf("Error: %s.\n", err.Error())
		}

		stream.Start()
		if err != nil {
			log.Fatalf("Error: %s.\n", err.Error())
		}

		log.Debugf("Listening...")
	listenLoop:
		for {
			select {
			case <-signalCh:
				log.Infof("Interrupt requested")
				break listenLoop
			case <-ticker.C:
				log.Debugf("Audio: %.2f kbit/s", float64(all*8)/1024)
				all = 0
			default:
				err = stream.Read()
				if err != nil {
					log.Error(err)
					break listenLoop
				}

				l := audioLevel(pcm)
				log.Debugf("Level: %d", l)

				var data []byte
				if doEncrypt {
					data, err = encryptor.Encrypt(pcm)
					if err != nil {
						log.Error(err)
						break listenLoop
					}
				} else {
					data = audiocrypt.FromPCM(pcm)
				}
				audioData <- audioPacket{data}

				all += len(data)
			}
		}
		kill <- true
		wg.Done()
	}()

	go func() {
		forwarder.Run()
		wg.Done()
	}()

	wg.Wait()
	log.Info("Terminated")
}
